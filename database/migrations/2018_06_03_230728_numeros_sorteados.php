<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NumerosSorteados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('numeros_sorteados', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('numero');
            $table->bigInteger('ordem');

            $table->unsignedInteger('sorteio_id');
            $table->foreign('sorteio_id')->references('id')->on('sorteios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('numeros_sorteados');
    }
}
