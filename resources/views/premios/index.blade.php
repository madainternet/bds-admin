@extends('layouts.app')

@section('content')    
    <h1>Prêmios disponíveis<button type="button" class="btn btn-success" onclick="window.location.href='./premios/create'">Novo</button></h1>  
    
    <hr />
    @if(count($premios) > 0)
        {{ $premios->links() }}
        @foreach($premios as $premio)
            <div class="well" id="premios_{{ $premio->id }}">
                <h3>{{$premio->nome}}</h3>
                <p>Status: <b>{{  $premio->getStatus() }}</b> </p>

                <button type="button" class="btn btn-warning" onclick="window.location.href='./premios/{{$premio->id}}/edit'">Editar</button>
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirmDelete" data-registro="{{ $premio->nome}}" data-control="premios" data-id="{{ $premio->id}}">Excluir</button>
            </div>
            <hr />
        @endforeach
        {{ $premios->links() }}

    @else
        <p>Nennhum Prêmio cadastrado</p>
    @endif
    <button type="button" class="btn btn-success" onclick="window.location.href='./premios/create'">Novo Prêmio</button>
    <hr>
@endsection