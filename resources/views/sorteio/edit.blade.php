@extends('layouts.app')

@section('content')
    <h1>Editar Sorteio</h1>
    
    {!! Form::open(['action' => ['SorteioController@update', $sorteio->id], 'method' => 'PUT', 'files' => true]) !!}    
        <div class="form-group">
            {{Form::label('ano','Ano')}}
            {{Form::text('ano',$sorteio->ano, ['class' => 'form-control', 'placeholder' => 'Ano'])}}
        </div>
        <div class="form-group">
            {{Form::label('numero','Número (Extração)')}}
            {{Form::number('numero',$sorteio->numero, ['class' => 'form-control', 'placeholder' => 'Nº (Extração) do Sorteio'])}}
        </div>
        
        <div class="form-group">
            {{ Form::label('data','Data')}}
            {{ Form::date('data', $sorteio->data, ['class' => 'form-control'])}}
        </div>

        <hr />

        <div class="form-group">
            {{ Form::label('cartela','Add. Cartela')}}
            <p>{{ Form::file('cartela') }}</p>
        </div> 

        <hr />

        @if(is_array($cartelas))
            <div class="grid">
            @foreach($cartelas as $cartela)
                <div class="well">
                    <img src="{{ url('storage/cartelas') }}/{{ $cartela->nome }}" width="220px" class="img-fluid img-thumbnail" /> 
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirmDelete" data-registro="{{ $cartela->nome}}" data-control="{{ url('file') }}" data-id="{{ $cartela->id}}">Excluir</button>
                </div>    
            @endforeach
            </div>
        @else
            <h3>Nenhuma cartela inserida</h3>
        @endif

        <hr />
        {{Form::hidden('_method','PUT')}}
        {{Form::submit('Salvar', ['class' => 'btn btn-primary', 'id' => 'btn-submit'])}}
            
    {!! Form::close() !!}

    <br class="clear" /><br class="clear" />
@endsection