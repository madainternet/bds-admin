@extends('layouts.app')

@section('content')    
    <h1>Pessoas <button type="button" class="btn btn-success" onclick="window.location.href='./pessoas/create'">Nova</button></h1>  
    
    <hr />
    @if(count($pessoas) > 0)
        {{ $pessoas->links() }}
        @foreach($pessoas as $pessoa)
            <div class="well" id="pessoas_{{ $pessoa->id }}">
                <h3>{{$pessoa->nome}}</h3>
                <p>Apelido: <b>{{ $pessoa->apelido}}</b></p>
                <p>Celular: <b>{{ $pessoa->celular}}</b> </p>
                <p>CEP:     <b>{{ $pessoa->cep}} </b> </p>
                
                <button type="button" class="btn btn-warning" onclick="window.location.href='./pessoas/{{$pessoa->id}}/edit'">Editar</button>
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirmDelete" data-registro="{{ $pessoa->nome}}" data-control="pessoas" data-id="{{ $pessoa->id}}">Excluir</button>
            </div>
            <hr />
        @endforeach
        {{ $pessoas->links() }}

    @else
        <p>Nennhuma pessoa cadastrada</p>
    @endif
    <button type="button" class="btn btn-success" onclick="window.location.href='./pessoas/create'">Nova pessoa</button>
    <hr>
@endsection
