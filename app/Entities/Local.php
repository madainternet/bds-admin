<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Local extends Model
{
    //use Notifiable;
    public $timestamps = false;
    protected $table = 'locais';
    protected $fillable = [
        'nome', 'link', 'status',
    ];

    public function getStatus(){
        return ($this->status === 'A') ? 'Ativo' : 'Inativo';
    }

}
