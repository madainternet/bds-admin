@extends('layouts.app')

@section('content')    
    <h1>Ganhadores <button type="button" class="btn btn-success" onclick="window.location.href='./ganhadores/create'">Novo</button></h1>  
    
    <hr />
    @if(count($ganhadores) > 0)
        {{ $ganhadores->links() }}
        @foreach($ganhadores as $ganhador)
            <div class="well" id="ganhadores_{{ $ganhador->id }}">
                <h3>{{$ganhador->pessoa['nome']}}</h3>
                <p>Sorteio:   <b>{{ $ganhador->sorteio->ano}}/{{ $ganhador->sorteio->numero}}</b></p>
                <p>Prêmio: <b>{{ $ganhador->premio->nome}}</b> </p>
                <p>Valor: <b>{{  $ganhador->valor }}</b> </p>
                
                <button type="button" class="btn btn-warning" onclick="window.location.href='./ganhadores/{{$ganhador->id}}/edit'">Editar</button>
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirmDelete" data-registro="{{ $ganhador->pessoa->nome}}" data-control="ganhadores" data-id="{{ $ganhador->id}}">Excluir</button>
            </div>
            <hr />
        @endforeach
        {{ $ganhadores->links() }}

    @else
        <p>Nennhum ganhador informado</p>
    @endif
    <button type="button" class="btn btn-success" onclick="window.location.href='./ganhadores/create'">Novo Ganhador</button>
    <hr>
@endsection
