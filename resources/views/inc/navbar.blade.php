<!--
<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">{{ config('app.name')}}</a>
    <input class="form-control form-control-dark w-100" type="text" placeholder="Procure por algo..." aria-label="Search">
    <ul class="navbar-nav px-6">
        <li class="nav-item text-nowrap">
            @guest
            <span class="badge badge-secondary"><a class="" href="{{ url('/login') }}">Entrar</a></span>
            @else
            <span class="badge badge-secondary">Logado como <b>{{ Auth::user()->nome }}</b></span>
            <a class="nav-link" href="#">Sair</a>
            <a class="nav-link px-3" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                Clique para Sair
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
            @endguest
        </li>
    </ul>
</nav>
-->
<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
    <div class="container">
        <a class="navbar-brand" href="{{ url('dashboard') }}">
             {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                @guest
                <li><a class="" href="{{ url('/login') }}">Entrar</a></li>
                @else
                    <li class="nav-link"> <span class="caret"></span></li>
                    <li class="nav-link">{{ Auth::user()->nome }}</li>
                    <li class="nav-link"><a class="nav-link" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">Sair</a></li>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>

                    <nav id="sidebar" class="col-md-2 d-none d-md-block bg-light sidebar">
                                <ul class="nav flex-column">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="{{ url('dashboard') }}">
                                            <span data-feather="home" icon="home"></span>
                                            Início <span class="sr-only">(current)</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ url('/locais') }}">
                                            <span data-feather="file"></span>
                                            Locais
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ url('/premios') }}">
                                            <span data-feather="file"></span>
                                            Prêmios
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ url('/sorteios') }}">
                                            <span data-feather="shopping-cart"></span>
                                            Sorteios
                                        </a>
                                    </li>
                                    
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ url('/banners') }}">
                                            <span data-feather="shopping-cart"></span>
                                            Banners
                                        </a>
                                    </li>
                                    
                                    <li class="dropdown-divider"></li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ url('/pessoas') }}">
                                            <span data-feather="users"></span>
                                            Pessoas
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ url('/ganhadores') }}">
                                            Ganhadores
                                        </a>
                                    </li>
                                    <li class="dropdown-divider"></li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">
                                            <span data-feather="bar-chart-2"></span>
                                            Relatórios
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                @endguest
                
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                
            </ul>
        </div>
    </div>
</nav>
