<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNumerosSorteados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        Schema::table('numeros_sorteados', function (Blueprint $table) {
            $table->unsignedInteger('premio_id')->default(1);
            $table->foreign('premio_id')->references('id')->on('premios');
        });
        */
        Schema::table('ganhadores', function (Blueprint $table) {
            $table->unsignedInteger('numero_sorteado_id')->nullable()->default(NULL);
            $table->foreign('numero_sorteado_id')->references('id')->on('numeros_sorteados');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('numeros_sorteados', function (Blueprint $table) {
            //
        });
    }
}
