<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Local;

class LocalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locais = Local::orderBy('nome', 'asc')->get();
        return view('local.index')->with('locais', $locais);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('local.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, 
            ['nome' => 'required','link' => 'required'],
            ['nome.required' => 'Campo Nome é Obrigatório', 'link.required' => 'Campo Link é Obrigatório']
        );

        $local = new Local();
        $local->nome   = $request->input('nome');
        $local->link   = $request->input('link');
        $local->status = 'A';

        $local->save();

        return response()->json(['success' => 'Local criado!', 'local' => $local->toArray()], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $local = Local::find($id);
        return view('local.edit')->with('local', $local);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nome' => 'required',
            'link' => 'required'
        ]);

        $local = Local::find($id);
        $local->nome   = $request->input('nome');
        $local->link   = $request->input('link');
        $local->status = 'A';

        $local->save();

        return redirect('/locais')->with('success', 'Local editado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            Local::destroy($id);
        }catch(Exception $e){
            return response()->json(['error' => $e->getMessage()], 500);
        }

        return response()->json(['success' => 'Local excluído'], 200);
    }
}
