@extends('layouts.app')

@section('content')
    <h1>Nova Pessoa</h1>
    {!! Form::open(['method' => 'POST', 'id'=>'form', 'data-control' => url('/pessoas') ]) !!}    
        <div class="form-group">
            {{Form::label('nome','Nome')}}
            {{Form::text('nome','', ['class' => 'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('apelido','Apelido')}}
            {{Form::text('apelido','', ['class' => 'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('celular','Celular')}}
            {{Form::text('celular','', ['class' => 'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('cep','CEP')}}
            {{Form::number('cep','', ['class' => 'form-control', 'placeholder' => 'CEP do Endereco'])}}
        </div>
        <div class="form-group">
            {{Form::label('local','Local do Cadastro')}}
            {{Form::select('local_cadastro_id', $locais, null, ['class' => 'form-control'])}}
        </div>
        
        {{Form::submit('Salvar', ['class' => 'btn btn-primary', 'id' => 'btn-submit'])}}    
    {!! Form::close() !!}
@endsection