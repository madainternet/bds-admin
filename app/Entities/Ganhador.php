<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Ganhador extends Model
{
    protected $table = 'ganhadores';
    protected $fillable = [
        'valor'
    ];

    public function pessoa(){
        return $this->belongsTo(Pessoa::class, 'pessoa_id');
    }

    public function sorteio(){
        return $this->belongsTo(Sorteio::class, 'sorteio_id');
    }

    public function premio(){
        return $this->belongsTo(Premio::class, 'premio_id');
    }
}
