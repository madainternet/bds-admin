<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/
Route::get('/', 'PagesController@index');


Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::resource('locais', 'LocalController');
    
    Route::resource('premios', 'PremioController');

    Route::resource('sorteios', 'SorteioController');
    Route::put('sorteios/alterarStatus/{sorteio_id}', 'SorteioController@alterarStatus');
    
    //Roda especifica
    Route::get('numeros/{premio_id}/{sorteio_id}', 'NumeroController@index');
    Route::post('numeros/{premio_id}/{sorteio_id}', 'NumeroController@store');
    Route::delete('numeros/{id}', 'NumeroController@destroy');
    Route::get('numeros/bySorteio/{sorteio_id}/{premio_id}', 'NumeroController@getBySorteio');

    Route::resource('pessoas', 'PessoaController');
    Route::get('pessoas/search/{term}', 'PessoaController@searchByTerm');

    Route::resource('ganhadores', 'GanhadorController');

    //Apagando arquivos
    Route::delete('file/{id}', 'FileController@destroy');
    
    Route::resource('banners', 'BannerController');

    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
});
