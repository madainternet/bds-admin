<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Pessoa;
use App\Entities\Local;

class PessoaController extends Controller
{

    public function index()
    {
        $total      = Pessoa::count();
        $pessoas    = Pessoa::orderBy('id','desc')->paginate(5);
       
        return view('pessoas.index')->with(['pessoas'=>$pessoas, 'total'=>$total]);
    }

    public function create(){
        $locais = Local::where('status', 'A')->orderBy('nome', 'asc')->pluck('nome', 'id');
        return view('pessoas.create')->with('locais', $locais);
    }

    public function store(Request $request)
    {
        $this->validate($request, 
            ['nome' => 'required','celular' => 'required', 'local_cadastro_id' => 'required'],
            ['nome.required'              => 'Nome é um campo obrigatório', 
             'celular.required'           => 'Celular é um campo Obrigatório',
             'local_cadastro_id.required' => 'Selecione um Local'
             ]
        );

        $pessoa = new Pessoa();
        $pessoa->nome      = $request->input('nome');
        $pessoa->apelido   = $request->input('apelido');
        $pessoa->celular   = $request->input('celular');
        $pessoa->cep       = trim($request->input('cep'));
        $pessoa->status    = 'A'; 
        $pessoa->local_cadastro_id = $request->input('local_cadastro_id');
        
        $pessoa->save();

        return response()->json(['success' => 'Pessoa criada!', 'pessoa' => $pessoa->toArray()], 200);
    }

    public function edit($id)
    {
        $pessoa = Pessoa::find($id);
        $locais = Local::where('status', 'A')->orderBy('nome', 'asc')->pluck('nome', 'id');
        
        return view('pessoas.edit')->with(['pessoa' => $pessoa, 'locais' => $locais]);
    }
    
    public function update(Request $request, $id)
    {
        $this->validate($request, 
            ['nome' => 'required','celular' => 'required', 'local_cadastro_id' => 'required'],
            ['nome.required'              => 'Nome é um campo obrigatório', 
             'celular.required'           => 'Celular é um campo Obrigatório',
             'local_cadastro_id.required' => 'Selecione um Local'
             ]
        );

        $pessoa = Pessoa::find($id);

        $pessoa->nome      = $request->input('nome');
        $pessoa->apelido   = $request->input('apelido');
        $pessoa->celular   = $request->input('celular');
        $pessoa->cep       = trim($request->input('cep')); 
        $pessoa->local_cadastro_id = $request->input('local_cadastro_id');

        $pessoa->save();

        return redirect('/pessoas')->with('success', 'Pessoa editada!');
    }


    public function destroy($id)
    {
        try{
            Pessoa::destroy($id);
        }catch(Exception $e){
            return response()->json(['error' => $e->getMessage()], 500);
        }

        return response()->json(['success' => 'Pessoa excluída'], 200);
    }
    

    public function searchByTerm($term){
        
        try{
            $pessoas = Pessoa::where([['nome','LIKE','%'.$term.'%']])
                              ->orWhere([['apelido','LIKE','%'.$term.'%']])->get();
            return response()->json(['results'=> $pessoas], 200);
        }catch (ModelNotFoundException $ex) {
            return response()->json(['error'=> $ex->getMessage()], 404);
        } catch (\Throwable $ex) {
            return response()->json(['error'=> $ex->getMessage()], 500);
        }
        
    }
}
