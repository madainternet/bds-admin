<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Numero extends Model
{
    protected $table = 'numeros_sorteados';
    protected $fillable = [
        'numero', 'sorteio_id', 'premio_id'
    ];
    public $timestamps = false;

    public function sorteio(){
        return $this->belongsTo(Sorteio::class, 'sorteio_id');
    }

    public function premio(){
        return $this->belongsTo(Premio::class, 'premio_id');
    }

}
