<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Premio extends Model
{
    protected $table = 'premios';
    public $timestamps = false;
    protected $fillable = ['nome','status'];

    /** Retornando dados formatados  */
    public function getStatus(){
        $r = '';
        switch($this->status){
            case 'A':
                $r = 'ATIVO';
                break;
            case 'I':
                $r = 'Inativo';
                break;
        }
        return $r;
    }
}
