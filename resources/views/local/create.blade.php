@extends('layouts.app')

@section('content')
    <h1>Novo Local</h1>
    <!-- 'action' => 'LocalController@store' -->
    {!! Form::open(['method' => 'POST', 'id'=>'form', 'data-control' => url('/locais') ]) !!}
    
        <div class="form-group">
            {{Form::label('nome','Nome')}}
            {{Form::text('nome','', ['class' => 'form-control', 'placeholder' => 'Nome do local'])}}
        </div>
        <div class="form-group">
            {{Form::label('link','Link (apenas o nome)')}}
            {{Form::text('link','', ['class' => 'form-control', 'placeholder' => 'Link (URL) de acesso'])}}
        </div>
        {{Form::submit('Salvar', ['class' => 'btn btn-primary', 'id' => 'btn-submit'])}}    
    {!! Form::close() !!}
@endsection