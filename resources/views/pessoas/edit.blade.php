@extends('layouts.app')

@section('content')
    <h1>Editar Pessoa</h1>
    {!! Form::open(['action' => ['PessoaController@update', $pessoa->id], 'method' => 'PUT']) !!}    
        <div class="form-group">
            {{Form::label('nome','Nome')}}
            {{Form::text('nome',$pessoa->nome, ['class' => 'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('apelido','Apelido')}}
            {{Form::text('apelido',$pessoa->apelido, ['class' => 'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('celular','Celular')}}
            {{Form::text('celular',$pessoa->celular, ['class' => 'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('cep','CEP')}}
            {{Form::number('cep',$pessoa->cep, ['class' => 'form-control', 'placeholder' => 'CEP do Endereco'])}}
        </div>
        <div class="form-group">
            {{Form::label('local','Local do Cadastro')}}
            {{Form::select('local_cadastro_id', $locais, $pessoa->local_cadastro_id, ['class' => 'form-control'])}}
        </div>
        {{Form::hidden('_method','PUT')}}
        
        {{Form::submit('Salvar', ['class' => 'btn btn-primary', 'id' => 'btn-submit'])}}    
    {!! Form::close() !!}
@endsection