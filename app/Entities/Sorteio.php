<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Sorteio extends Model
{
    protected $table = 'sorteios';
    protected $fillable = [
        'ano', 'numero', 'status', 'data'
    ];

    public function local(){
        return $this->belongsTo(Local::class, 'local_id');
    }
    /** Retornando dados formatados  */
    public function getStatus(){
        $r = '';
        switch($this->status){
            case 'AT':
                $r = 'Aguardando transmissão';
                break;
            case 'T':
                $r = 'Transmitido';
                break;
        }
        return $r;
    }

    public function getData(){
        $data = new \DateTime($this->data);
        return $data->format('d/m/Y');
    }

    public function numeros(){
        return $this->hasMany(Numero::class);
    }

}
