@extends('layouts.app')

@section('content')
    <h1>Novo Ganhador</h1>
    {!! Form::open(['method' => 'POST', 'id'=>'form', 'data-control' => url('/ganhadores')]) !!}
        
        <div class="form-group">
            {{Form::label('local','Local')}}
            {{Form::select('local_id', $locais, null, ['class' => 'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('sorteio','Sorteio')}}
            {{Form::select('sorteio_id', $sorteios, null, ['class' => 'form-control', 'id'=> 'sorteio_id'])}}
        </div>
        <div class="form-group autocomplete">
            {{Form::label('buscar_pessoa','Pessoa')}}
            {{Form::text('buscar_pessoa','', ['id'=>'searching', 'autocomplete'=>'off', 'data-control'=>url('/pessoas'), 'data-action'=>'search', 'data-returnid'=>'#pessoa_id',  'class' => 'form-control', 'placeholder' => 'Procure um pessoa cadastrada...'])}}
        </div>


        
        <div class="form-group">
            {{Form::label('premio','Tipo do Prêmio')}}
            {{Form::select('premio_id', [false=>'Escolha um sorteio antes'], null, ['class' => 'form-control', 'id'=>'premio_id'])}}
        </div>

        <div id="numeros" class="form-group" style="display:none;">
 
            {{Form::label('numero_sorteado_id','Número Sorteado*', ['data-toggle' => 'tooltip', 'title' => 'Número da batida ou do Giro da Sorte'])}}
 
            {{Form::select('numero_sorteado_id', [null=>'Escolha um prêmio antes'], null, ['class' => 'form-control', 'id'=>'numero_sorteado_id'])}}
        </div>

        <div class="form-group">
            {{Form::label('valor','Valor Recebido')}}
            {{Form::text('valor','', ['class' => 'form-control', 'placeholder' => 'Ex.: 1.000,00'])}}
        </div>

        {{ Form::hidden('pessoa_id', null, ['id'=>'pessoa_id'])}}
       
        {{Form::submit('Salvar', ['class' => 'btn btn-primary', 'id' => 'btn-submit'])}}    
        <br clear="all" /><br clear="all" />
    {!! Form::close() !!}

    <script>
    $('#sorteio_id').on('change', function(){
        var id = $(this).val();
        
        $.ajax({
            url: "{{ url('sorteios/') }}/"+id,
            headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr('content')},
            type: 'get'
        }).done(function(result){
            if(result !== null){
                resetSelect('#premio_id');
                $.each(result.data, function(i, item){
                    $('#premio_id').append('<option value="'+i+'">'+item+'</option>');
                });
            }
        }).fail(function(reject){
            console.log(reject);
        });
    });



    $('#premio_id').on('change', function(){
        var sorteio_id = $('#sorteio_id').val();
        var premio_id  = $('#premio_id').val();

        if(sorteio_id === null){
            alert('Escolha um sorteio antes!');
            return false;
        }

        $.ajax({
            url: "{{ url('numeros/bySorteio') }}/"+sorteio_id+'/'+premio_id,
            headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr('content')},
            type: 'get'
        }).done(function(result){
            if(result !== null){
                resetSelect('#numero_sorteado_id');
                $.each(result.data, function(i, item){
                    $('#numero_sorteado_id').append('<option value="'+item.id+'">Ordem: '+item.ordem+' | Número: '+item.numero+'</option>');
                });

                $('#numeros').show();
            }
        }).fail(function(reject){
            console.log(reject);
        });
    });

    
    var resetSelect = function(element){
        $(element).find('option').remove().end();
    };


    </script>
@endsection