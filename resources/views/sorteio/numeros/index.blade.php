@extends('layouts.app')

@section('content')
<h2>Numeros do Sorteio {{ $sorteio->numero.'/'.$sorteio->ano }}</h2>
@if(!isset($premios))
    <b style="color:green">{{ $total }} números no total</b>
        <hr />
        <p class="success">Adicionar números</p>
        {!! Form::open(['method' => 'POST', 'id'=>'form', 'data-control' => url('./numeros/'.$premio->id.'/'.$sorteio->id) ]) !!}
            <div class="form-group">
                {{Form::label('ordem','Ordem chamada')}}
                {{Form::number('ordem','', ['class' => 'form-control', 'placeholder' => 'Ordem', 'size'=>5, 'autofocus'])}}

                {{Form::label('numero','Número')}}
                {{Form::number('numero','', ['class' => 'form-control', 'placeholder' => 'Nº', 'size'=>5])}}

            </div> 
            {{ Form::submit('Salvar', ['class' => 'btn btn-primary', 'id' => 'btn-submit'])}}
            {{ Form::button('VOLTAR', ['class' => 'btn btn-danger', 'onclick' => 'window.history.back(-1)'])}}    
        {!! Form::close() !!}
        <hr />
        @if(count($numeros) > 0)
            {{ $numeros->links() }}
            <ul class="list-group">
            @foreach($numeros as $numero)
                <li class="list-group-item" id="{{ url('numeros') }}_{{ $numero->id }}"> 
                    Número: <b>{{ $numero->numero }}</b> | Ordem chamada: <b>{{ $numero->ordem }}</b>
                    <button type="button" class="btn-sm btn-danger" data-toggle="modal" data-target="#confirmDelete" data-registro="Nº {{ $numero->numero}}" data-control="{{ url('numeros') }}" data-id="{{ $numero->id}}">Excluir</button>
                </li>
            @endforeach
            {{ $numeros->links() }}
        @else
            <h3>Nenhum número informado para o {{ $premio->nome }}</h3>
        @endif    
        <hr />
@else
    <div class="form-group">
        {{Form::label('premios','Selecione um prêmio')}}
        {{Form::select('premios', isset($premios) ? $premios : [], null, ['class' => 'form-control', 'id'=>'premio_select'])}}
    </div>
    
    {{ Form::button('Carregar', ['class' => 'btn btn-primary', 'onclick' => 'carregarNumerosPremio()']) }}

@endif            
    <script>
        var carregarNumerosPremio = function(){
            var premio_id = $('#premio_select').val();
            window.location.href = "{{ url('numeros')}}/"+premio_id+"/{{ $sorteio->id }}";
        };
    </script>
@endsection
