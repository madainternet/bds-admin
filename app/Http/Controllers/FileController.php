<?php

namespace App\Http\Controllers;

use App\Entities\File;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class FileController extends Controller
{
    public function saveFile($model, $id, $folder, $filename, UploadedFile $uploadFile){
        
        if($this->upload($uploadFile, $folder, $filename)){
            $file = new File();
            
            $file->nome     = $filename.'.'.$uploadFile->extension();
            $file->model    = $model;
            $file->model_id = $id;
            $file->size     = $uploadFile->getClientSize();

            try{
                $file->save();
            }catch(Exception $e){
                return response()->json(['error'=>$e->getMessage()], 500);
            }

            return true;
        }

        
    }

    public function destroy($id)
    {
        try{
            File::destroy($id);
        }catch(Exception $e){
            return response()->json(['error' => $e->getMessage()], 500);
        }

        return response()->json(['success' => 'Arquivo excluído'], 200);
    }

    private function upload(UploadedFile $uploadFile, $folder, $filename){
        $filename = $filename.'.'.$uploadFile->extension();
        try{
            $uploadFile->storeAs('public/'.$folder, $filename);
        }catch(Exception $e){
            return false;
        }

        return true;
    }

}
