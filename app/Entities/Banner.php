<?php

namespace App\Entities;

use App\Entities\Banner;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = 'banners';
    protected $fillable = [
        'imagem',
        'posicao',
        'ordem'
    ];
}
