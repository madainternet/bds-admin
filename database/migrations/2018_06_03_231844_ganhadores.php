<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Ganhadores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ganhadores', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('sorteio_id');
            $table->foreign('sorteio_id')->references('id')->on('sorteios');

            $table->unsignedInteger('pessoa_id');
            $table->foreign('pessoa_id')->references('id')->on('pessoas');

            $table->unsignedInteger('premio_id');
            $table->foreign('premio_id')->references('id')->on('premios');

            $table->char('valor', 10);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ganhadores');
    }
}
