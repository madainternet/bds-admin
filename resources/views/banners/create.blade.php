@extends('layouts.app')

@section('content')
    <h1>Novo Banner</h1>
    
    {!! Form::open(['method' => 'POST', 'files' => true, 'action'=> 'BannerController@store' ]) !!}    
        <div class="form-group">
            {{Form::label('posicao','Local')}}
            {{Form::select('posicao',['T'=>'TOP'], null, ['class' => 'form-control'])}}
        </div>

        <div class="form-group">
            {{Form::label('ordem','Ordem')}}
            {{Form::text('nextpos', $nextpos, ['class' => 'form-control', 'disabled'=>true])}}
        </div>

        <div class="form-group">
            {{ Form::label('banner','Banner (Imagem)')}}
            <p>{{ Form::file('bannerFile') }}</p>
        </div>

          {{Form::hidden('ordem', $nextpos)}}
        {{Form::submit('Salvar', ['class' => 'btn btn-primary', 'id' => 'btn-submit'])}}
            
    {!! Form::close() !!}

    <br class="clear" /><br class="clear" />
@endsection


