@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <p>Transmissão AO VIVO está <b style="color:red;">inativa</b> agora. 
                        <a href="#">Clique aqui para ATIVAR</a><p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
