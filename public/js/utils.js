$(document).ready(function(){

    $('#confirmDelete').on('show.bs.modal', function (event) {
        var button     = $(event.relatedTarget); 
        
        var registro   = button.data('registro');
        var controller = button.data('control');
        var id         = button.data('id');

        var modal = $(this);
        modal.find('.modal-title').text('Confirmar remoção (' + registro + ')');
        
        var action     = new actions();

        $('#btn-confirm').on('click', function(){
            action.delete(controller, id).then(function(result){
                if(result.success){  
                    toast('success', result.message, 3000);
                    modal.modal('hide');
                    $('#'+controller+'_'+id).hide('slow');
                } 
            });
        });
    });

    $('#confirmAction').on('show.bs.modal', function (event) {
        var button     = $(event.relatedTarget); 
        
        var nomeacao   = button.data('nomeacao');
        var controller = button.data('control');
        var acao       = button.data('acao');
        var dados      = button.data('dados');
        
        var modal = $(this);
        modal.find('.modal-title').text('Confirmar ' + nomeacao);
        
        var action     = new actions();
        
        $('#btn-confirm-action').on('click', function(){
            action.specified(controller, acao, dados).then(function(result){
                if(result.success){  
                    toast('success', result.message, 2000);
                    setTimeout(location.reload.bind(location), 2000);
                } 
            });
        });
    });

    $('#form').on('submit', function (event) {
        $('#btn-submit').val('Processando...');
        var form       = $(this);
        
        var controller = form.data('control');
        var data       = $(this).serialize();
        
        var action     = new actions();
        action.insert(controller, data).then(function(result){
            if(result.success){
                toast('success', result.message, 1000, controller);
                $('#btn-submit').val('Salvar');
            }
        }).catch(function(result){
            if(!result.success){  
                var msg = '';
                $.each(result.retorno.errors, function(index, value) {
                    msg += value+' <br />';
                });
                
                toast('error', msg, 3000);
                $('#btn-submit').val('Salvar');
            }
        });

        return false;
    });

    $('#searching').keyup(function(e) {
        var controller = $(this).attr('data-control');
        var act        = $(this).attr('data-action');
        var returnid   = $(this).attr('data-returnid');

        if(action === undefined) var action = new actions();

        clearTimeout($.data(this, 'timer'));
        
        if (e.keyCode == 13){
            search(true, action, controller, act, returnid);
        }else{
            $(this).data('timer', setTimeout(search(false, action, controller, act, returnid)));
        }
          
    });

    //$('#data').datetimepicker();    
});

function toast(action, msg, duration, redir = false){
    var x     = document.getElementById("toast-msg");
    var color = '';
    
    switch(action){
        case 'success':
            color = 'green';
            break;
        case 'error':
            color = 'red';
            break;
    }
    
    x.className = "show";
    $('#toast-msg').html(msg);
    $('#toast-msg').css('background-color', color);

    setTimeout(function(){ 
        x.className = x.className.replace("show", ""); 
    }, duration);

    if(redir) window.location.href=redir;
};

function search(force, action, controller, act, returnid){
    var term = $('#searching').val();
    if (!force && term.length < 3) return;
    
    action.searchInField(force, controller, act, term).then(function(result){
        autocomplete(document.getElementById("searching"), result);
        $(returnid).val(result[(result.length - 1)].id);
    });
};