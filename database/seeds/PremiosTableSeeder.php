<?php

use Illuminate\Database\Seeder;

class PremiosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('premios')->insert([
            [
            'nome'   => 'Princial',
            'status' => 'A',
            ],
            [
            'nome'   => 'Giro da Sorte',
            'status' => 'A',
            ]
        ]);
    }
}
