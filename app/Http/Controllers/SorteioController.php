<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Premio;
use App\Entities\Sorteio;
use App\Entities\Local;
use App\Entities\File;
use App\Entities\Numero;

class SorteioController extends Controller
{
    protected $folderCartelas = 'cartelas';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sorteios = Sorteio::orderBy('id', 'desc')->paginate(5);
        return view('sorteio.index')->with(['sorteios'=> $sorteios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locais = Local::where('status', 'A')->orderBy('nome', 'asc')->pluck('nome', 'id');
        return view('sorteio.create')->with('locais', $locais);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, 
            ['ano' => 'required','numero' => 'required', 'local_id' => 'required', 'data'=> 'required'],
            ['ano.required' => 'Ano é um campo obrigatório', 
             'numero.required' => 'Número é um campo Obrigatório',
             'local_id.required' => 'Selecione um Local',
             'data.required' => 'Informe uma data',
             ]
        );

        $sorteio = new Sorteio();
        $sorteio->ano      = $request->input('ano');
        $sorteio->numero   = $request->input('numero');
        $sorteio->local_id = $request->input('local_id');
        $sorteio->data     = $request->input('data');
        $sorteio->status   = 'AT'; // Aguardando transmissão

        $sorteio->save();

        return response()->json(['success' => 'Sorteio criado!', 'sorteio' => $sorteio->toArray()], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sorteio = Sorteio::find($id);
        $numeros = $sorteio->numeros()->orderBy('ordem', 'asc')->get();

        $premios = [];

        if(is_object($numeros)){
            foreach($numeros as $numero){
                if(!isset($premios[$numero->premio_id])){
                    $premios[$numero->premio_id] = $numero->premio->nome;
                }
            }
        }
        

        if(request()->wantsJson()){
            return response()->json([
                'data'=> $premios
            ]);
        }

        return response()->json([
            'data'=> $premios
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sorteio = Sorteio::find($id);
        $files   = File::where([['model', '=', 'Sorteio'], ['model_id', '=', $id]])->get();
        
        return view('sorteio.edit')->with(['sorteio' => $sorteio, 'cartelas' => $files->all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'ano' => 'required',
            'numero' => 'required'
        ]);
            
        $sorteio = Sorteio::find($id);
        $sorteio->ano    = $request->input('ano');
        $sorteio->numero = $request->input('numero');
        $sorteio->data   = $request->input('data');

        $sorteio->save();

        if($request->hasFile('cartela')){
            $filename = $sorteio->ano.$sorteio->numero.'_'.md5(microtime());
            app('App\Http\Controllers\FileController')->saveFile('Sorteio', $sorteio->id, $this->folderCartelas, $filename, $request->file('cartela'));
        }

        return redirect('/sorteios')->with('success', 'Sorteio editado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            Sorteio::destroy($id);
        }catch(Exception $e){
            return response()->json(['error' => $e->getMessage()], 500);
        }

        return response()->json(['success' => 'Sorteio excluído'], 200);
    }

    public function alterarStatus(Request $request, $id){
        $sorteio = Sorteio::find($id);

        $sorteio->status = $request->input('status');
        $sorteio->save();

        return response()->json(['success' => 'Sorteio marcado como transmitido!', 'sorteio' => $sorteio->toArray()], 200);
    }

    /** */

    private function saveCartela($filename){
        $file = new File();
        $file->nome = $filename;
        $file->model = $model;
        $file->model_id = $id;

        try{
            $file->save();
        }catch(Exception $e){
            return response()->json(['error'=>$e->getMessage()], 500);
        }

        return true;

    }
}
