@extends('layouts.app')

@section('content')
    <h1>Editando Prêmio</h1>
    {!! Form::open(['action' => ['PremioController@update', $premio->id], 'method' => 'PUT']) !!}    
        <div class="form-group">
            {{Form::label('nome','Nome')}}
            {{Form::text('nome', $premio->nome, ['class' => 'form-control', 'placeholder' => 'Nome do prêmio...'])}}
        </div>

        <div class="form-group">
            {{Form::label('status','Status')}}
            {{Form::select('status', ['A' => 'ATIVO', 'I'=>'Inativo'], $premio->status, ['class' => 'form-control'])}}
        </div>
        {{Form::hidden('_method','PUT')}}
        
        {{Form::submit('Salvar', ['class' => 'btn btn-primary', 'id' => 'btn-submit'])}}
        
    {!! Form::close() !!}
@endsection