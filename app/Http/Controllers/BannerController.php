<?php

namespace App\Http\Controllers;

use App\Entities\Banner;
use App\Entities\File;

use Illuminate\Http\Request;

class BannerController extends Controller
{
    protected $folderBanners = 'banners';

    public function index()
    {
        $banners = Banner::orderBy('ordem', 'asc')
                    ->join('files', function ($join) {
                        $join->on('banners.id', '=', 'files.model_id')
                            ->where('files.model', '=', 'Banner');
                    })
                    ->select('files.nome as imagem', 'banners.ordem as ordem', 'banners.id as id')
                    ->get();

        return view('banners.index')->with(['banners'=> $banners]);
    }

    public function create(){
        $posicao = Banner::orderBy('ordem', 'desc')->first();
        $ordem   = (is_object($posicao)) ? $posicao->ordem + 1 : 1;

        return view('banners.create')->with(['nextpos'=> $ordem]);
    }

    public function store(Request $request){

        $this->validate($request, 
            ['posicao' => 'required'],
            ['posicao.required' => 'Posição do Banner é obrigatória']
        );

        try{
            
            if($request->hasFile('bannerFile')){
                $banner           = new Banner();
                $banner->imagem   = 'banner_'.date('YmdHis').'_'.$request->input('posicao');
                $banner->posicao  = $request->input('posicao');
                $banner->ordem    = $request->input('ordem');
                $banner->save();

                $filename = $banner->imagem;
                app('App\Http\Controllers\FileController')->saveFile('Banner', $banner->id, $this->folderBanners, $filename, $request->file('bannerFile'));
                
                return redirect('/banners')->with('success', 'Banner criado!');
            }else{
                return response()->json(['errors' => ['É necessário escolher o banner']], 442);
            }

        }catch(Exception $e){
            return response()->json(['error'=>$e->getMessage()], 500);
        }
    }

    public function destroy($id)
    {
        $banners = Banner::orderBy('ordem','desc')->first();
        $banner  = Banner::find($id);
        
        try{
            Banner::destroy($id);
            $file = File::where([['model', '=', 'Banner'],['model_id', '=', $id]])->first();
            File::destroy($file->id);

            for($x = ($banner->ordem + 1); $x <= $banners->ordem; $x++){
                $banner_update[$x] = Banner::where('ordem', $x)->update(['ordem' => $x - 1]);
            }

        }catch(Exception $e){
            return response()->json(['error' => $e->getMessage()], 500);
        }
        
        /*
        echo $banners->ordem; 
        echo '<br>';
        echo $banner->ordem;
        */      
        


        return response()->json(['success' => 'Arquivo excluído'], 200);
    }
}
