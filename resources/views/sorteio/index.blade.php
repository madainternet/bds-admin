@extends('layouts.app')

@section('content')    
    <h1>Sorteios realizados <button type="button" class="btn btn-success" onclick="window.location.href='./sorteios/create'">Novo</button></h1>  
    
    <hr />
    @if(count($sorteios) > 0)
        {{ $sorteios->links() }}
        @foreach($sorteios as $sorteio)
            <div class="well" id="sorteios_{{ $sorteio->id }}">
                <h3>Sorteio: {{$sorteio->ano}}/{{$sorteio->numero}}</h3>
                <p>Local:   <b>{{ $sorteio->local->nome}}</b></p>
                <p>Data: <b>{{ $sorteio->getData() }}</b> </p>
                <p>Status: <b>{{  $sorteio->getStatus() }}</b> </p>
                @if($sorteio->status == 'AT')
                <button type="button" class="btn btn-secondary" 
                        data-toggle="modal" 
                        data-target="#confirmAction" 
                        data-nomeacao="Marcar como Transmitido" 
                        data-control="sorteios" 
                        data-acao="alterarStatus/{{ $sorteio->id}}"
                        data-dados={"status":"T"}>Marcar como transmitido</button> <br /> <br />
                @endif
                
                <button type="button" class="btn btn-warning" onclick="window.location.href='./sorteios/{{$sorteio->id}}/edit'">Editar</button>
                
                <button type="button" class="btn btn-primary" onclick="window.location.href='./numeros/null/{{$sorteio->id}}/'">Gerenciar Prêmios</button>
                
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirmDelete" data-registro="{{ $sorteio->ano}}/{{ $sorteio->numero}}" data-control="sorteios" data-id="{{ $sorteio->id}}">Excluir</button>
            </div>
            <hr />
        @endforeach
        {{ $sorteios->links() }}

    @else
        <p>Nennhum sorteio realizado</p>
    @endif
    <button type="button" class="btn btn-success" onclick="window.location.href='./sorteios/create'">Novo Sorteio</button>
    <hr>
@endsection