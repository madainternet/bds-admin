<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Entities\Premio;
use App\Entities\Sorteio;
use App\Entities\Numero;

class NumeroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($premio_id, $sorteio_id)
    {
        $premio_id = ($premio_id === 'null') ? null : $premio_id;

        $opcs = [];
        $opcs['sorteio']  = Sorteio::find($sorteio_id);

        if(!is_null($premio_id)) {
            $opcs['premio']  = Premio::find($premio_id);
            $opcs['total']   = Numero::where(['sorteio_id'=>$sorteio_id, 'premio_id' => $premio_id])->get()->count();
            $opcs['numeros'] = Numero::where(['sorteio_id'=>$sorteio_id, 'premio_id' => $premio_id])->orderBy('ordem','desc')->paginate(5);
        }else{
            $opcs['premios'] = Premio::where('status', 'A')->orderBy('nome', 'asc')->pluck('nome', 'id');
        } 
        
        return view('sorteio.numeros.index')->with($opcs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locais = Local::where('status', 'A')->orderBy('nome', 'asc')->pluck('nome', 'id');
        return view('sorteio.create')->with('locais', $locais);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $premio_id, $sorteio_id)
    {
        $this->validate($request, 
            ['numero' => 'required','ordem' => 'required'],
            ['numero.required' => 'Número é um campo Obrigatório', 
             'ordem.required' => 'Ordem é necessária'
             ]
        );

        $numero = new Numero();
        $numero->numero     = $request->input('numero');
        $numero->ordem      = $request->input('ordem');
        $numero->sorteio_id = $sorteio_id;
        $numero->premio_id  = $premio_id;
        
        try{
            $numero->save();
        }catch(\Illuminate\Database\QueryException $e){
            if(strstr($e->getMessage(), 'unique_ordem_numero')){
                $msg = 'Este número nessa ordem já foi inserido neste prêmio deste sorteio';
            }elseif(strstr($e->getMessage(), 'unique_ordem')){
                $msg = 'Essa ordem já foi inserida neste prêmio deste sorteio';
            }elseif(strstr($e->getMessage(), 'unique_numero')){
                $msg = 'Esse número já foi inserido neste prêmio deste sorteio';
            }else{
                $msg = $e->getMessage();
            }

            return response()->json(['errors'=>[$msg]], 442);
        }

        return response()->json(['success' => 'Número adicionado!', 'numero' => $numero->toArray()], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function getBySorteio($sorteio_id, $premio_id = null){
        $numeros = Numero::where([['sorteio_id','=', $sorteio_id], ['premio_id', '=', $premio_id]])->get();

        return response()->json([
            'data'=> $numeros
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sorteio = Sorteio::find($id);
        return view('sorteio.edit')->with('sorteio', $sorteio);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nome' => 'required',
            'link' => 'required'
        ]);

        $local = Local::find($id);
        $local->nome   = $request->input('nome');
        $local->link   = $request->input('link');
        $local->status = 'A';

        $local->save();

        return redirect('/locais')->with('success', 'Local editado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            Numero::destroy($id);
        }catch(Exception $e){
            return response()->json(['error' => $e->getMessage()], 500);
        }

        return response()->json(['success' => 'Número excluído'], 200);
    }

}
