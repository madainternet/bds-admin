@extends('layouts.app')

@section('content')    
    <h1>Banners<button type="button" class="btn btn-success" onclick="window.location.href='./banners/create'">Novo</button></h1>  
    
    <hr />
    @if(count($banners) > 0)
        @foreach($banners as $banner)
            <div class="well" id="banners_{{ $banner->id }}">
                <b>Banner {{ $banner->ordem}}</b>
                <img src="{{ url('storage/banners') }}/{{ $banner->imagem }}" class="img-fluid img-thumbnail" />
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirmDelete" data-registro="{{ $banner->imagem}}" data-control="banners" data-id="{{ $banner->id}}">Remover</button>   
            </div>
            <hr />
        @endforeach

    @else
        <p>Nennhum Banner cadastrado</p>
    @endif
    <button type="button" class="btn btn-success" onclick="window.location.href='./banners/create'">Novo Banner</button>
    <hr>
@endsection