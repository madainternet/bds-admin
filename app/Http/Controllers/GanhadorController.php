<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Ganhador;
use App\Entities\Local;
use App\Entities\Sorteio;
use App\Entities\Premio;
use App\Entities\File;

class GanhadorController extends Controller
{
    protected $folderGanhadores = 'ganhadores';

    public function index()
    {
        $total      = Ganhador::count();
        $ganhadores = Ganhador::where([['valor','>',0]])->with('pessoa')->orderBy('id','desc')->paginate(5);
       
        return view('ganhadores.index')->with(['ganhadores'=>$ganhadores, 'total'=>$total]);
    }

    public function create(){
        $locais    = Local::where('status', 'A')->orderBy('nome', 'asc')->pluck('nome', 'id');
        
        $total_sorteios = Sorteio::count();
        $psorteios = Sorteio::orderBy('id', 'asc')->get();
        
        $sorteios = [];

        if($total_sorteios > 0){
            $sorteios[null] = '::Escolha um sorteio::';
            foreach($psorteios as $ch=>$psorteio){
                $sorteios[$psorteio->id] = $psorteio->ano.'/'.$psorteio->numero;
            }
        }

        return view('ganhadores.create')->with(['locais' => $locais, 
                                                'sorteios' => $sorteios]);
    }

    public function store(Request $request){

        $this->validate($request, 
            ['local_id'   => 'required',
             'sorteio_id' => 'required',
             'pessoa_id'  => 'required',
             'valor'      => 'required'],
            
            ['local_id.required'   => 'Local é um campo Obrigatório', 
             'sorteio_id.required' => 'Sorteio é obrigatório',
             'pessoa_id.required'  => 'É necessário encontrar uma pessoa antes de inserir',
             'valor.required'      => 'Valor do Prêmio para o Ganhador é necesssário'
            ]
        );

        $ganhador = new Ganhador();
        
        $ganhador->sorteio_id  = $request->input('sorteio_id');
        $ganhador->pessoa_id   = $request->input('pessoa_id');
        $ganhador->premio_id   = $request->input('premio_id');
        $ganhador->numero_sorteado_id   = $request->input('numero_sorteado_id');
        $ganhador->valor     = $request->input('valor');
        
        try{
            $ganhador->save();
        }catch(\Illuminate\Database\QueryException $e){
            if(strstr($e->getMessage(), 'valor')){
                $msg = 'É necessário informar o valor recebido ganhador';
            }elseif(strstr($e->getMessage(), 'unique_ordem')){
                $msg = 'Essa ordem já foi inserida neste prêmio deste sorteio';
            }elseif(strstr($e->getMessage(), 'unique_numero')){
                $msg = 'Esse número já foi inserido neste prêmio deste sorteio';
            }else{
                $msg = $e->getMessage();
            }

            return response()->json(['errors'=>[$msg]], 442);
        }

        return response()->json(['success' => 'Ganhador adicionado!', 'ganhador' => $ganhador->toArray()], 200);
    }

    public function edit($id)
    {
        $ganhador = Ganhador::find($id);
        $locais    = Local::where('status', 'A')->orderBy('nome', 'asc')->pluck('nome', 'id');
        
        $total_sorteios = Sorteio::count();
        $psorteios = Sorteio::orderBy('id', 'asc')->get();
        
        $sorteios = [];

        if($total_sorteios > 0){
            $sorteios[null] = '::Escolha um sorteio::';
            foreach($psorteios as $ch=>$psorteio){
                $sorteios[$psorteio->id] = $psorteio->ano.'/'.$psorteio->numero;
            }
        }

        $files   = File::where([['model', '=', 'Ganhador'], ['model_id', '=', $id]])->get();

        return view('ganhadores.edit')->with(['ganhador' => $ganhador,
                                              'locais'   => $locais, 
                                              'sorteios' => $sorteios,
                                              'fotos'    => $files->all()]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, 
            ['local_id'   => 'required',
             'sorteio_id' => 'required',
             'valor'      => 'required'],
            
            ['local_id.required'   => 'Local é um campo Obrigatório', 
             'sorteio_id.required' => 'Sorteio é obrigatório',
             'valor.required'      => 'Valor do Prêmio para o Ganhador é necesssário'
            ]
        );

        $ganhador = Ganhador::find($id);
        
        $ganhador->sorteio_id  = $request->input('sorteio_id');
        $ganhador->premio_id   = $request->input('premio_id');
        $ganhador->numero_sorteado_id   = $request->input('numero_sorteado_id');
        $ganhador->valor     = $request->input('valor');

        $ganhador->save();

        if($request->hasFile('foto')){
            $filename = strtoupper(str_replace(' ','_', $ganhador->pessoa->nome)).'_'.$ganhador->id.'_'.md5(microtime());
            app('App\Http\Controllers\FileController')->saveFile('Ganhador', $ganhador->id, $this->folderGanhadores, $filename, $request->file('foto'));
        }

        return redirect('/ganhadores')->with('success', 'Ganhador editado!');
    }

    public function destroy($id){
        try{
            File::where([['model', '=', 'Ganhador'], ['model_id', '=', $id]])->delete();
            Ganhador::destroy($id);
        }catch(Exception $e){
            return response()->json(['error' => $e->getMessage()], 500);
        }

        return response()->json(['success' => 'Ganhador excluído'], 200);
    }
}
