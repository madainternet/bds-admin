<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nome'   => 'Administrador',
            'email'   => 'admin@brasildasorte.com.br',
            'status'  => 'A',
            'password' => bcrypt('bds123'), // secret
            'remember_token' => str_random(10),
        ]);
    }
}
