@extends('layouts.app')

@section('content')
    <h1>Novo Prêmio</h1>
    {!! Form::open(['method' => 'POST', 'id'=>'form', 'data-control' => url('/premios') ]) !!}    
        <div class="form-group">
            {{Form::label('nome','Nome')}}
            {{Form::text('nome', null, ['class' => 'form-control', 'placeholder' => 'Nome do prêmio...'])}}
        </div>
        
        {{Form::submit('Salvar', ['class' => 'btn btn-primary', 'id' => 'btn-submit'])}}    
    {!! Form::close() !!}
@endsection