@extends('layouts.app')

@section('content')
    <h1>Editar Local ({{ $local->nome }})</h1>
    {!! Form::open(['action' => ['LocalController@update', $local->id], 'method' => 'POST']) !!}
        <div class="form-group">
            {{Form::label('nome','Nome')}}
            {{Form::text('nome',$local->nome, ['class' => 'form-control', 'placeholder' => 'Nome do local'])}}
        </div>
        <div class="form-group">
            {{Form::label('link','Link (apenas o nome)')}}
            {{Form::text('link',$local->link, ['class' => 'form-control', 'placeholder' => 'Link (URL) de acesso'])}}
        </div>
        {{Form::hidden('_method','PUT')}}    
        {{Form::submit('Salvar', ['class' => 'btn btn-primary'])}}    
    {!! Form::close() !!}
@endsection