@extends('layouts.app')

@section('content')
    <h1>Novo Ganhador</h1>
    
    {!! Form::open(['action' => ['GanhadorController@update', $ganhador->id], 'method' => 'PUT', 'files' => true]) !!} 

        <div class="form-group">
            {{Form::label('local','Local')}}
            {{Form::select('local_id', $locais, $ganhador->pessoa->local_cadastro_id, ['class' => 'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('sorteio','Sorteio')}}
            {{Form::select('sorteio_id', $sorteios, $ganhador->sorteio->id, ['class' => 'form-control', 'id'=> 'sorteio_id'])}}
        </div>
        
        <div class="form-group" id="pessoa">
            {{Form::label('pessoa','Pessoa:')}}
            <b>{{ $ganhador->pessoa->nome}}</b>
        </div>

        <div class="form-group">
            {{Form::label('premio','Tipo do Prêmio')}}
            {{Form::select('premio_id', [false=>'Escolha um sorteio antes'], $ganhador->premio->id, ['class' => 'form-control', 'id'=>'premio_id'])}}
        </div>

        <div id="numeros" class="form-group" style="display:none;">
 
            {{Form::label('numero_sorteado_id','Número Sorteado*', ['data-toggle' => 'tooltip', 'title' => 'Número da batida ou do Giro da Sorte'])}}
 
            {{Form::select('numero_sorteado_id', [null=>'Escolha um prêmio antes'], $ganhador->numero_sorteado_id, ['class' => 'form-control', 'id'=>'numero_sorteado_id'])}}
        </div>

        <div class="form-group">
            {{Form::label('valor','Valor Recebido')}}
            {{Form::text('valor', $ganhador->valor, ['class' => 'form-control', 'placeholder' => 'Ex.: 1.000,00'])}}
        </div>
        
        <div class="form-group">
            {{Form::label('foto','Foto')}}
            {{Form::file('foto', ['class' => 'form-control'])}}
        </div>

        {{ Form::hidden('pessoa_id', null, ['id'=>'pessoa_id'])}}
        {{ Form::hidden('_method', 'PUT')}}


        @if(is_array($fotos))
            <div class="grid">
            @foreach($fotos as $foto)
                <div class="well">
                    <img src="{{ url('storage/ganhadores') }}/{{ $foto->nome }}" width="220px" class="img-fluid img-thumbnail" /> 
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirmDelete" data-registro="{{ $foto->nome}}" data-control="{{ url('file') }}" data-id="{{ $foto->id}}">Excluir</button>
                </div>    
            @endforeach
            </div>
        @else
            <h3>Nenhuma foto inserida</h3>
        @endif

        {{Form::submit('Salvar', ['class' => 'btn btn-primary', 'id' => 'btn-submit'])}}    
        <br clear="all" /><br clear="all" />
    {!! Form::close() !!}

    <script>
    $('#sorteio_id').on('change', function(){
        getPremios($(this).val());
    });

    $('#premio_id').on('change', function(){
        getNumeroSorteados($('#sorteio_id').val());
    });

    
    var resetSelect = function(element){
        $(element).find('option').remove().end();
    };

    var getNumeroSorteados = function(sorteio_id, premio_id){
        var sorteio_id = sorteio_id;
        var premio_id  = premio_id;

        if(sorteio_id === null){
            alert('Escolha um sorteio antes!');
            return false;
        }

        $.ajax({
            url: "{{ url('numeros/bySorteio') }}/"+sorteio_id+'/'+premio_id,
            headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr('content')},
            type: 'get'
        }).done(function(result){
            if(result !== null){
                resetSelect('#numero_sorteado_id');
                $.each(result.data, function(i, item){
                    $('#numero_sorteado_id').append('<option value="'+item.id+'">Ordem: '+item.ordem+' | Número: '+item.numero+'</option>');
                });

                $('#numeros').show();
            }
        }).fail(function(reject){
            console.log(reject);
        });
    };

    var getPremios = function(sorteio_id){
        var id = sorteio_id;
        
        $.ajax({
            url: "{{ url('sorteios/') }}/"+id,
            headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr('content')},
            type: 'get'
        }).done(function(result){
            if(result !== null){
                resetSelect('#premio_id');
                $.each(result.data, function(i, item){
                    $('#premio_id').append('<option value="'+i+'">'+item+'</option>');
                });
            }
        }).fail(function(reject){
            console.log(reject);
        });
    }


    getPremios('{{ $ganhador->sorteio->id }}');
    getNumeroSorteados('{{ $ganhador->sorteio->id }}', '{{ $ganhador->premio->id }}')
    </script>
@endsection