function actions(){};
actions.prototype.delete = function(controller, id){
    return new Promise((resolve, reject) => {
        $.ajax({
            method:'DELETE',
            headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr('content')},
            url: controller+'/'+id
        }).done(function(data){
            resolve({success:true, message: data.success});
        }).fail(function(x,v,f){
            console.log(x,v,f);
        });
    })
};

actions.prototype.insert = function(controller, data){
    return new Promise((resolve, reject) => {
        $.ajax({
            method:'POST',
            //headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr('content')},
            url: controller,
            data: data
        }).done(function(data){
            resolve({success:true, message: data.success});
        }).fail(function(data){
            reject({success:false, retorno:data.responseJSON});
        });
    })
};

actions.prototype.searchInField = function(force, controller, action,  term){
    return new Promise((resolve, reject) => {
        $.get(controller+'/'+action+'/' + term, function(data) {
            resolve(data.results);
        });
    });
};

actions.prototype.specified = function(controller, action, data){
    
    return new Promise((resolve, reject) => {
        $.ajax({
            type:'PUT',
            headers: {"X-CSRF-TOKEN":$('meta[name="csrf-token"]').attr('content')},
            url: controller+'/'+action,
            data: data
        }).done(function(data){
            resolve({success:true, message: data.success});
        }).fail(function(data){
            reject({success:false, retorno:data.responseJSON});
        });
    })
};