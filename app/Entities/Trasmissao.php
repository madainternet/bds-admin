<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Trasmissao extends Model
{
    //use Notifiable;
    public $timestamps = false;
    protected $table = 'transmissoes';
    protected $fillable = [
        'nome', 'link', 'status',
    ];//
}
