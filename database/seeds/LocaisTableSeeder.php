<?php

use Illuminate\Database\Seeder;
use App\Entities\Local;

class LocaisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('locais')->insert([
            'nome'   => 'Tucuruí',
            'link'   => 'tucurui',
            'status' => 'A',
        ]);
    }
}
