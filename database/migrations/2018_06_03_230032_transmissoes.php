<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Transmissoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transmissoes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('chave')->unique();
            $table->date('data');
            $table->char('status', 1);

            $table->unsignedInteger('sorteio_id');
            $table->foreign('sorteio_id')->references('id')->on('sorteios');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transmissoes');
    }
}
