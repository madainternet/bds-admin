<?php

namespace App\Http\Controllers;

use App\Entities\Premio;
use Illuminate\Http\Request;

class PremioController extends Controller
{
    public function index()
    {
        $premios = Premio::orderBy('id', 'desc')->paginate(5);
        return view('premios.index')->with(['premios'=> $premios]);
    }

    public function create()
    {
        return view('premios.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, 
            ['nome' => 'required'],
            ['nome.required' => 'O Nome é um campo obrigatório']
        );

        $premio = new Premio();
        $premio->nome      = $request->input('nome');
        $premio->status    = 'A'; // ATIVO  
        $premio->save();

        return response()->json(['success' => 'Prêmio criado!', 'premio' => $premio->toArray()], 200);
    }

    public function edit($id)
    {
        $premio = Premio::find($id);
        return view('premios.edit')->with('premio', $premio);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nome' => 'required',
            'status' => 'required'
        ]);

        $premio = Premio::find($id);
        $premio->nome    = $request->input('nome');
        $premio->status  = $request->input('status');
        
        $premio->save();

        return redirect('/premios')->with('success', 'Prêmio editado!');
    }

    public function destroy($id)
    {
        try{
            Premio::destroy($id);
        }catch(Exception $e){
            return response()->json(['error' => $e->getMessage()], 500);
        }catch(\Illuminate\Database\QueryException $e){
            if(strstr($e->getMessage(), 'numeros_sorteados_premio_id_foreign')){
                $msg = 'Esse prêmio já está relacionado a algum sorteio. Ele não pode ser excluído';
            }

            return response()->json(['errors'=>[$msg]], 442);
        }

        return response()->json(['success' => 'Sorteio excluído'], 200);
    }
}
