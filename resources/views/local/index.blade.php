@extends('layouts.app')

@section('content')    
    <h1>Locais existentes <button type="button" class="btn btn-success" onclick="window.location.href='./locais/create'">Cadastrar</button></h1>  
    
    <hr />
    @if(count($locais) > 0)
        @foreach($locais as $local)
            <div class="well" id="locais_{{ $local->id }}">
                <h3>{{$local->nome}}</h3>
                <p>Endereço: <a href="http://www.brasildasorte.com.br/{{ $local->link}}" target="_blank">www.brasildasorte.com.br/{{ $local->link}}</a></p>
                <p>Situação: <b>{{ $local->getStatus() }}</b> </p>
                <button type="button" class="btn btn-warning" onclick="window.location.href='./locais/{{$local->id}}/edit'">Editar</button>
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirmDelete" data-registro="{{ $local->nome}}" data-control="locais" data-id="{{ $local->id}}">Excluir</button>
            </div>
            <hr />
        @endforeach

    @else
        <p>Nennhum local inserido</p>
    @endif
    <button type="button" class="btn btn-success" onclick="window.location.href='./locais/create'">Cadastrar Novo</button>
    <hr>
@endsection