@extends('layouts.app')

@section('content')
    <h1>Novo Sorteio</h1>
    <!-- 'action' => 'LocalController@store' -->
    {!! Form::open(['method' => 'POST', 'id'=>'form', 'data-control' => url('/sorteios') ]) !!}    
        <div class="form-group">
            {{Form::label('ano','Ano')}}
            {{Form::text('ano',date('Y'), ['class' => 'form-control', 'placeholder' => 'Ano'])}}
        </div>
        <div class="form-group">
            {{Form::label('numero','Número (Extração)')}}
            {{Form::number('numero','', ['class' => 'form-control', 'placeholder' => 'Nº (Extração) do Sorteio'])}}
        </div>
        <div class="form-group">
            {{Form::label('local','Local do Sorteio')}}
            {{Form::select('local_id', $locais, null, ['class' => 'form-control'])}}
        </div>
        <div class="form-group">
            {{ Form::label('data','Data')}}
            {{ Form::date('data', \Carbon\Carbon::now(), ['class' => 'form-control'])}}
        </div>
        
        {{Form::submit('Salvar', ['class' => 'btn btn-primary', 'id' => 'btn-submit'])}}    
    {!! Form::close() !!}
@endsection