<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model
{
    protected $table = 'pessoas';

    protected $fillable = [
        'nome', 
        'apelido',
        'celular',
        'cep',
        'status',
        'local_cadastro_id'
    ];

    public function local(){
        return $this->belongTo(Local::class, 'local_cadastro_id');
    }

    public function ganhador(){
        return $this->hasMany(Ganhador::class);
    }
    //
}
